using System;
using Godot;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public static class DataManager {
    public static T Load<T>(string path) where T : JToken, new() {
        var file = new File();
        var dataFile = file.Open(path, File.ModeFlags.Read);
        if (dataFile == Error.Ok) {
            try {
                var text = file.GetAsText();
                file.Close();
                return (T) JToken.Parse(text);
            } catch (Exception ex) {
                GD.PrintErr(ex.StackTrace);
                file.Close();
                return new T();
            }
        }
        return new T();
    }

    public static void Save(string path, JToken data, bool isSpecial = true) {
        var file = new File();
        var dataFile = file.Open(path, File.ModeFlags.Write);
        if (dataFile == Error.Ok) {
            file.StoreString(data.ToString());
            file.Close();
        }
        GD.Print($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}] Saved {path}");
        if (!isSpecial) return;

        foreach (JSONData item in data) {
            item.Dirty = false;
        }
    }
}

using Godot;

public class ReleaseCard : Control {
    [Export] NodePath nameLabelNode;
    [Export] NodePath typeLabelNode;
    [Export] NodePath urlLabelNode;
    [Export] NodePath typeContainerNode;
    [Export] NodePath monoIndicatorNode;
    [Export] NodePath buttonNode;
    [Export] NodePath tagsContainerNode;
    [Export] PackedScene tagScene;

    Label typeLabel;
    Label nameLabel;
    Label urlLabel;
    Control monoIndicator;
    Control typeContainer;
    Control tagsContainer;
    Release release;
    string version;

    public override void _EnterTree() {
        base._EnterTree();
        typeLabel = GetNode<Label>(typeLabelNode);
        monoIndicator = GetNode<Control>(monoIndicatorNode);
        typeContainer = GetNode<Control>(typeContainerNode);
        nameLabel = GetNode<Label>(nameLabelNode);
        urlLabel = GetNode<Label>(urlLabelNode);
        tagsContainer = GetNode<Control>(tagsContainerNode);
        GetNode<Button>(buttonNode).Connect("pressed", this, nameof(Download));
    }

    public void Setup(Release release, string version) {
        this.release = release;
        this.version = version;
        nameLabel.Text = release.Name;
        typeLabel.Text = release.Type;
        urlLabel.Text = release.URL;
        typeContainer.Visible = !string.IsNullOrWhiteSpace(release.Type);
        monoIndicator.Visible = release.Mono;
        foreach (var tag in release.Tags) {
            AddTag(tag);
        }
    }

    void AddTag(string tag) {
        var tagParent = tagScene.Instance<Control>();
        tagsContainer.AddChild(tagParent);
        tagParent.GetNode<Label>("Tag").Text = tag;
    }

    void Download() {
        var install = new Install {
            Version = version,
            URL = release.URL,
            Mono = release.Mono,
            ReleaseType = release.Type,
        };
        Main.Inst.StartDownload(install);
    }
}

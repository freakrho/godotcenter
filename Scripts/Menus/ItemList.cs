using Godot;

public class ItemList : Node {
    [Signal] public delegate void Modified();

    [Export] NodePath rootNode;
    [Export] NodePath addButtonNode;
    [Export] PackedScene itemScene;
    Node root;

    public int Count => root.GetChildCount();
    public ListItem this[int i] => root.GetChild<ListItem>(i);

    public override void _EnterTree() {
        base._EnterTree();
        root = GetNode(rootNode);
        GetNode<Button>(addButtonNode).Connect("pressed", this, nameof(Add));
    }

    public void Add() {
        AddItem<Node>();
    }

    public T AddItem<T>() where T : Node {
        var item = itemScene.Instance<T>();
        root.AddChild(item);
        item.Connect(nameof(ListItem.Modified), this, nameof(ModifiedChild));
        EmitSignal(nameof(Modified));
        return item;
    }
    
    public void Clear() {
        for (int i = 0; i < root.GetChildCount(); i++) {
            root.GetChild(i).QueueFree();
        }
    }

    void ModifiedChild() {
        EmitSignal(nameof(Modified));
    }
}

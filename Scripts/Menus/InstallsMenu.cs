using System.Collections.Generic;
using Godot;
using Newtonsoft.Json.Linq;

public class InstallsMenu : Control {

    [Export] PackedScene installDataScene;
    [Export] NodePath installsRootNode;
    [Export] NodePath addLocalMenuNode;

    Node installsRoot;
    AddLocalMenu addLocalMenu;


    public override void _EnterTree() {
        base._EnterTree();
        Installs.Load();
    }

    public override void _Ready() {
        base._Ready();

        installsRoot = GetNode(installsRootNode);
        addLocalMenu = GetNode<AddLocalMenu>(addLocalMenuNode);
        Installs.OnUpdate += RefreshInstallsList;
        RefreshInstallsList();
    }

    public void RefreshInstallsList() {
        foreach (var node in installsRoot.GetChildren()) {
            (node as Node).QueueFree();
        }

        foreach (Install install in Installs.GetInstalls()) {
            if (string.IsNullOrWhiteSpace(install.ExecutablePath)) {
                install.ExecutablePath = DownloadProgressMenu.FindExecutable(
                    install.InstallPath);
            }
            var node = installDataScene.Instance<InstallData>();
            installsRoot.AddChild(node);
            node.Setup(install);
        }

        Installs.Save();
    }


    public void OpenAddLocalMenu() {
        addLocalMenu.Visible = true;
    }
}

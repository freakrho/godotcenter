using Godot;

public class ProjectsMenu : Control {
    [Export] NodePath projectListRootNode;
    [Export] PackedScene projectScene;
    [Export] NodePath fileDialogNode;
    [Export] NodePath addLocalButtonNode;
    [Export] NodePath createProjectButtonNode;
    [Export] NodePath createProjectPanelNode;
    Node projectsRoot;
    FileDialog fileDialog;
    CreateProjectPanel createProjectPanel;

    public override void _EnterTree() {
        base._EnterTree();
        projectsRoot = GetNode(projectListRootNode);
        fileDialog = GetNode<FileDialog>(fileDialogNode);
        var addButton = GetNode<Button>(addLocalButtonNode);
        addButton.Connect("pressed", this, nameof(AddLocalProjectPrompt));
        GetNode<Button>(createProjectButtonNode).OnPressed(this, nameof(OpenCreateProjectPanel));
        createProjectPanel = GetNode<CreateProjectPanel>(createProjectPanelNode);
        createProjectPanel.Visible = false;
    }

    public override void _Ready() {
        base._Ready();
        Projects.Load();
        Projects.OnUpdate += RefreshProjectList;
        RefreshProjectList();
    }

    void RefreshProjectList() {
        projectsRoot.QueueFreeChildren();

        foreach (Project project in Projects.Enumerable) {
            var projectData = projectScene.Instance<ProjectData>();
            projectsRoot.AddChild(projectData);
            projectData.Setup(project);
        }

        if (Projects.Dirty) {
            Projects.Save();
        }
    }

    void AddLocalProjectPrompt() {
        fileDialog.SelectFile(this, nameof(AddProject), nameof(ClosedAddProjectDialog));
        fileDialog.CurrentDir = "user://";
        fileDialog.Filters = new string[] {"*.godot;Godot projects"};
    }

    void ClosedAddProjectDialog() {
        fileDialog.DisconnectDir(this, nameof(AddProject), nameof(ClosedAddProjectDialog));
    }

    void AddProject(string path) {
        var project = new Project();
        project.ProjectPath = path;
        if (Installs.Count > 0) {
            project.VersionID = Installs.Get(0).ID;
        }
        Projects.Add(project);
    }

    void OpenCreateProjectPanel() {
        createProjectPanel.ShowPanel();
    }
}

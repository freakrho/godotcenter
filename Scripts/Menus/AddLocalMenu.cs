using Godot;

public class AddLocalMenu : Control {
    [Export] NodePath versionNode;
    [Export] NodePath monoNode;
    [Export] NodePath installPathNode;
    [Export] NodePath executablePathNode;
    [Export] NodePath folderDialogNode;
    [Export] NodePath fileDialogNode;
    [Export] NodePath tagsRootNode;

    LineEdit version;
    Button mono;
    LineEdit installPath;
    LineEdit executablePath;
    FileDialog folderDialog;
    FileDialog fileDialog;
    Node tagsRoot;

    public override void _EnterTree() {
        base._EnterTree();
        version = GetNode<LineEdit>(versionNode);
        mono = GetNode<Button>(monoNode);
        installPath = GetNode<LineEdit>(installPathNode);
        executablePath = GetNode<LineEdit>(executablePathNode);
        folderDialog = GetNode<FileDialog>(folderDialogNode);
        fileDialog = GetNode<FileDialog>(fileDialogNode);
        tagsRoot = GetNode(tagsRootNode);
    }

    public override void _Ready() {
        base._Ready();
        AddTagField();
    }

    public void ChooseInstallPath() {
        folderDialog.SelectDir(this, nameof(SetInstallPath), nameof(ClosedInstallPopup));
        folderDialog.CurrentDir = "user://";
    }

    public void ChooseExecutablePath() {
        fileDialog.SelectFile(this, nameof(SetExecutable), nameof(ClosedExecutablePopup));
        if (string.IsNullOrWhiteSpace(installPath.Text)) {
            fileDialog.CurrentDir = "user://";
        } else {
            fileDialog.CurrentDir = installPath.Text;
        }
    }

    void ClosedInstallPopup() {
        folderDialog.DisconnectDir(this, nameof(SetInstallPath), nameof(ClosedInstallPopup));
    }
    void ClosedExecutablePopup() {
        fileDialog.DisconnectFile(this, nameof(SetExecutable), nameof(ClosedExecutablePopup));
    }

    public void SetInstallPath(string dir) {
        installPath.Text = dir;
        ClosedInstallPopup();
    }

    public void SetExecutable(string dir) {
        executablePath.Text = dir;
    }

    public void AddTagField() {
        var field = new LineEdit();
        tagsRoot.AddChild(field);
    }

    public void Close() {
        Hide();
    }

    public void Add() {
        var install = new Install();
        install.Version = version.Text;
        install.ExecutablePath = executablePath.Text;
        install.InstallPath = installPath.Text;
        install.Mono = mono.Pressed;
        for (int i = 0; i < tagsRoot.GetChildCount(); i++) {
            var tag = tagsRoot.GetChild<LineEdit>(i).Text;
            if (!string.IsNullOrWhiteSpace(tag)) {
                install.AddTag(tag);
            }
        }
        Installs.AddInstall(install);
        Visible = false;
    }
}

using Godot;

public class InstallData : Control {
    [Export] NodePath versionLabelNode;
    [Export] NodePath pathLabelNode;
    [Export] NodePath deleteButtonNode;
    [Export] NodePath pathButtonNode;
    [Export] NodePath tagsContainerNode;
    [Export] NodePath defaultToggleNode;
    [Export] PackedScene tagScene;

    Label versionLabel;
    Label pathLabel;
    Control tagsContainer;
    CheckBox defaultToggle;

    Install install;

    static event System.Action OnUpdateDefault;

    public override void _Ready() {
        base._Ready();

        versionLabel = GetNode<Label>(versionLabelNode);
        pathLabel = GetNode<Label>(pathLabelNode);
        tagsContainer = GetNode<Control>(tagsContainerNode);
        var deleteButton = GetNode<Button>(deleteButtonNode);
        deleteButton.Connect("pressed", this, nameof(Delete));
        var pathButton = GetNode<Button>(pathButtonNode);
        pathButton.Connect("pressed", this, nameof(OpenDir));
        defaultToggle = GetNode<CheckBox>(defaultToggleNode);
        defaultToggle.Connect("toggled", this, nameof(SetDefault));
        OnUpdateDefault += UpdateDefault;
    }

    public override void _ExitTree() {
        base._ExitTree();
        OnUpdateDefault -= UpdateDefault;
    }

    void Delete() {
        Main.Inst.ShowConfirmation($"Are you sure you want to delete v{install.Version}?",
            (confirmed) => {
                if (confirmed) {
                    Installs.DeleteInstall(install);   
                }
            }
        );
    }

    void OpenDir() {
        OS.ShellOpen(install.InstallPath);
    }

    public void Setup(Install install) {
        this.install = install;
        
        pathLabel.Text = install.InstallPath;
        versionLabel.Text = $"v{install.Version}";
        
        if (!string.IsNullOrEmpty(install.ReleaseType)) {
            AddTag(install.ReleaseType);
        }
        if (install.Mono) {
            AddTag("Mono");
        }

        for (int i = 0; i < install.TagsLength; i++) {
            AddTag(install.GetTag(i));
        }
        UpdateDefault();
    }

    void AddTag(string tag) {
        var tagParent = tagScene.Instance<Control>();
        tagsContainer.AddChild(tagParent);
        tagParent.GetNode<Label>("Tag").Text = tag;
    }

    void SetDefault(bool state) {
        if (state) {
            Settings.DefaultInstall = install.ID;
            Settings.Save();
            OnUpdateDefault?.Invoke();
        }
    }

    void UpdateDefault() {
        defaultToggle.Pressed = Settings.DefaultInstall == install.ID;
    }
}

using Godot;
using System.Collections.Generic;
using System.Text;

public enum EngineVersion {
    None,
    Linux32,
    Linux64,
    Linux64Headless,
    Windows32,
    Windows64,
    OSX,
    LinuxServer,
    Android,
}

public class Request {
    public string Url;
    public System.Action<bool, string> Callback;
}

public class Main : Node {
    public const string mirror = "https://downloads.tuxfamily.org/godotengine";
    const string DOWNLOAD_PATH = "temp/tmp_godot.zip";

    public static Main Inst { get; private set; }

    [Signal] delegate void OnDownloadStep(float progress);

    [Export] NodePath menuControlNode;
    [Export] NodePath chooseVersionPopupNode;
    
    // Nodes
    MenuControl menuControl;
    ChooseVersionPopup chooseVersionPopup;
    bool saveSettings;

    public override void _EnterTree() {
        base._EnterTree();
        Inst = this;

        var name = ProjectSettings.GetSetting("application/config/name");
        var version = ProjectSettings.GetSetting("application/config/version");
        OS.SetWindowTitle($"{name} v{version}");
    }

    public override void _Ready() {
        chooseVersionPopup = GetNode<ChooseVersionPopup>(chooseVersionPopupNode);
        menuControl = GetNode<MenuControl>(menuControlNode);

        menuControl.ShowProjects();
        chooseVersionPopup.Visible = false;
    }

    public override void _Process(float delta) {
        base._Process(delta);
        if (saveSettings) {
            Settings.DoSave();
            saveSettings = false;
        }
    }

    public void SaveSettings() {
        saveSettings = true;
    }
    
    public void OpenDownloadMenu() {
        chooseVersionPopup.Visible = true;
    }

    public void StartDownload(Install install) {
        chooseVersionPopup.DownloadRelease(install);
    }

    public void ShowConfirmation(string text, System.Action<bool> callback) {
        menuControl.ShowConfirmation(text, callback);
    }
}

using Godot;

public class VersionList : PanelContainer {
    [Export] NodePath rootNode;
    [Export] NodePath versionLabelNode;
    [Export] NodePath versionListToggleNode;
    [Export] NodePath versionTypeContainerNode;
    [Export] NodePath versionTypeLabelNode;
    [Export] PackedScene releaseItem;
    Control root;
    Label versionLabel;
    Label versionTypeLabel;
    Control versionTypeContainer;
    Button versionListToggle;

    public override void _EnterTree() {
        base._EnterTree();
        root = GetNode<Control>(rootNode);
        versionLabel = GetNode<Label>(versionLabelNode);
        versionListToggle = GetNode<Button>(versionListToggleNode);
        versionListToggle.Connect("toggled", this, nameof(Toggled));
        versionTypeLabel = GetNode<Label>(versionTypeLabelNode);
        versionTypeContainer = GetNode<Control>(versionTypeContainerNode);
    }

    void Toggled(bool value) {
        root.Visible = value;
    }

    int GetHighestRC(Release release, string previous) {
        int highest = 0;
        if (previous.StartsWith("RC")) {
            if (int.TryParse(previous.Substring(2), out var rc)) {
                highest = rc;
            }
        }
        foreach (var tag in release.Tags) {
            if (tag.StartsWith("RC")) {
                if (int.TryParse(tag.Substring(2), out var rc)) {
                    if (rc > highest) {
                        highest = rc;
                    }
                }
            }
        }
        return highest;
    }

    string VersionType(Release release, string previous) {
        if (previous == "Stable") return previous;
        if (release.Type == "Stable") return release.Type;
        if (previous == "Release") return previous;
        if (release.Type == "Release") return release.Type;
        var rc = GetHighestRC(release, previous);
        if (rc > 0) return $"RC{rc}";
        if (previous == "Beta") return previous;
        if (release.Type == "Beta") return release.Type;
        if (previous == "Alpha") return previous;
        if (release.Type == "Alpha") return release.Type;

        return previous;
    }

    public void SetVersion(VersionRemote remote) {
        versionLabel.Text = $"v{remote.Version}";
        var type = "";
        foreach (var release in remote.Releases) {
            if (!release.Done) continue;
            AddRelease(release, remote.Version);
            type = VersionType(release, type);
        }
        versionListToggle.Pressed = false;
        root.Visible = false;
        versionTypeLabel.Text = type;
        versionTypeContainer.Visible = !string.IsNullOrWhiteSpace(type);
    }

    public void AddRelease(Release release, string version) {
        var item = releaseItem.Instance<ReleaseCard>();
        root.AddChild(item);
        item.Setup(release, version);
    }
}

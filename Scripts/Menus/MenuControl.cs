using Godot;

public class MenuControl : Control {
    [Export] NodePath projectsNode;
    [Export] NodePath installsNode;
    [Export] NodePath settingsNode;
    [Export] NodePath confirmationDialogNode;

    ProjectsMenu projectsMenu;
    public InstallsMenu InstallsMenu { get; private set; }
    Control settingsMenu;
    Control[] allControls;
    ConfirmationDialog confirmationDialog;
    System.Action<bool> confirmationCallback;

    public override void _EnterTree() {
        base._EnterTree();

        projectsMenu = GetNode<ProjectsMenu>(projectsNode);
        InstallsMenu = GetNode<InstallsMenu>(installsNode);
        settingsMenu = GetNode<Control>(settingsNode);

        allControls = new Control[] {
            projectsMenu,
            InstallsMenu,
            settingsMenu,
        };

        confirmationDialog = GetNode<ConfirmationDialog>(confirmationDialogNode);
        confirmationDialog.Connect("confirmed", this, nameof(Confirm));
        confirmationDialog.Connect("popup_hide", this, nameof(Cancel));
    }

    public void ShowMenu(Control menu) {
        foreach (var control in allControls) {
            control.Visible = false;
        }
        menu.Visible = true;
    }

    public void ShowProjects() {
        ShowMenu(projectsMenu);
    }

    public void ShowInstalls() {
        ShowMenu(InstallsMenu);
    }

    public void ShowSettings() {
        ShowMenu(settingsMenu);
    }

    public void ShowConfirmation(string text, System.Action<bool> callback) {
        confirmationCallback = callback;
        confirmationDialog.DialogText = text;
        confirmationDialog.Show();
    }

    void Confirm() {
        confirmationCallback?.Invoke(true);
    }

    void Cancel() {
        confirmationCallback?.Invoke(false);
    }
}

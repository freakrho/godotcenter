using Godot;

public class ProjectData : PanelContainer {
    [Export] NodePath projectNameNode;
    [Export] NodePath projectPathNode;
    [Export] NodePath versionSelectorNode;
    [Export] NodePath launchButtonNode;
    [Export] NodePath openDirButtonNode;
    [Export] NodePath favoriteButtonNode;
    [Export] NodePath removeButtonNode;
    [Export] NodePath modifiedLabelNode;
    Label projectNameLabel;
    Label projectPathLabel;
    Label modifiedLabel;
    OptionButton versionSelector;
    Project project;
    string[] installIDs;
    CheckBox favoriteButton;

    public override void _EnterTree() {
        base._EnterTree();
        projectNameLabel = GetNode<Label>(projectNameNode);
        projectPathLabel = GetNode<Label>(projectPathNode);
        modifiedLabel = GetNode<Label>(modifiedLabelNode);
        versionSelector = GetNode<OptionButton>(versionSelectorNode);
        versionSelector.Connect("item_selected", this, nameof(OnChangeVersion));
        GetNode<Button>(launchButtonNode).OnPressed(this, nameof(Launch));
        GetNode<Button>(openDirButtonNode).OnPressed(this, nameof(OpenDir));
        GetNode<Button>(removeButtonNode).OnPressed(this, nameof(Remove));
        favoriteButton = GetNode<CheckBox>(favoriteButtonNode);
        favoriteButton.Connect("toggled", this, nameof(SetFavorite));

        Installs.OnUpdate += Setup;
    }

    void OnChangeVersion(int item) {
        project.VersionID = installIDs[item];
        Projects.Save();
    }

    public void Launch() {
        var install = Installs.GetInstall(project.VersionID);
        if (install != null) {
            LaunchProject(install, project.ProjectPath);
            project.ModifiedNow();
            Projects.MakeFirst(project);
            Projects.Save();
        }
    }

    public void OpenDir() {
        OS.ShellOpen(System.IO.Path.GetDirectoryName(project.ProjectPath));
    }

    public void Setup() {
        projectNameLabel.Text = project.ProjectName;
        projectPathLabel.Text = project.ProjectPath;
        for (int j = versionSelector.Items.Count - 1; j >= 0; j--) {
            versionSelector.RemoveItem(j);
        }
        installIDs = new string[Installs.Count];
        versionSelector.Disabled = Installs.Count == 0;
        if (Installs.Count == 0) return;

        int i = 0;
        foreach (var install in Installs.GetInstalls()) {
            installIDs[i] = install.ID;
            versionSelector.AddItem(install.FullVersionName);
            if (install.ID == project.VersionID) {
                versionSelector.Selected = i;
            }
            i++;
        }
        if (versionSelector.Selected < 0) {
            versionSelector.Selected = 0;
            project.VersionID = installIDs[0];
        }
        favoriteButton.Pressed = project.Favorite;
        modifiedLabel.Text = project.TimeAgo();
    }

    public void Setup(Project project) {
        this.project = project;
        Setup();
    }

    public static void LaunchProject(Install install, string path) {
        GD.Print($"Execute {install.ExecutablePath} \"{path}\"");
        OS.Execute(install.ExecutablePath, new string[] { path }, blocking: false);
    }

    void SetFavorite(bool value) {
        project.Favorite = value;
        Projects.Save();
    }

    void Remove() {
        Projects.Remove(project);
        Projects.Save();
    }
}

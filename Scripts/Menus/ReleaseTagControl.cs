using Godot;

public class ReleaseTagControl : ListItem {
    [Export] NodePath nameNode;
    [Export] NodePath keyListNode;
    ItemList keyList;

    public event System.Action OnModified;
    public LineEdit TagName;
    public int Count => keyList.Count;
    public ListItem this[int i] => keyList[i];

    public override void _EnterTree() {
        base._EnterTree();
        keyList = GetNode<ItemList>(keyListNode);
        TagName = GetNode<LineEdit>(nameNode);
        TagName.Connect("text_entered", this, nameof(ModifiedName));
        keyList.Connect(nameof(ItemList.Modified), this, nameof(ModifiedList));
    }

    public ReleaseTag GetTag() {
        var keys = new string[keyList.Count];
        for (int i = 0; i < keys.Length; i++) {
            keys[i] = (keyList[i].Data as LineEdit).Text;
        }
        return new ReleaseTag {
            Name = TagName.Text,
            SearchKeys = keys,
        };
    }

    public void SetTag(ReleaseTag tag) {
        TagName.Text = tag.Name;
        keyList.Clear();
        foreach (var key in tag.SearchKeys) {
            var item = keyList.AddItem<ListItem>();
            (item.Data as LineEdit).Text = key;
        }
    }

    void ModifiedName(string text) {
        EmitSignal(nameof(Modified));
    }

    void ModifiedList() {
        EmitSignal(nameof(Modified));
    }
}

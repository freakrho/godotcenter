using Godot;

public class SettingsMenu : Control {
    [Export] NodePath folderDialogNode;
    [Export] NodePath storagePathLabelNode;
    [Export] NodePath tagListNode;
    [Export] NodePath openSettingsFileButtonNode;

    FileDialog folderDialog;
    LineEdit storagePathLabel;
    ItemList tagList;

    void LoadNodes() {
        folderDialog = GetNode<FileDialog>(folderDialogNode);
        storagePathLabel = GetNode<LineEdit>(storagePathLabelNode);
        storagePathLabel.Connect("text_entered", this, nameof(SetDir));
        tagList = GetNode<ItemList>(tagListNode);

        var openSettingsFileButton = GetNode<Button>(openSettingsFileButtonNode);
        openSettingsFileButton.Connect("pressed", this, nameof(OpenSettingsFile));
    }

    public override void _Ready() {
        base._Ready();
        LoadNodes();

        storagePathLabel.Text = Settings.StorePath;
        foreach (var tag in Settings.Tags()) {
            var tagControl = tagList.AddItem<ReleaseTagControl>();
            tagControl.SetTag(tag);
            tagControl.Connect(nameof(ReleaseTagControl.Modified), this, nameof(ModifiedTag));
        }

        tagList.Connect(nameof(ItemList.Modified), this, nameof(ModifiedTag));
    }

    void OpenSettingsFile() {
        GD.Print(OS.GetUserDataDir());
        OS.ShellOpen(OS.GetUserDataDir());
    }

    public void ChooseStoragePath() {
        folderDialog.SelectDir(this, nameof(SetDirDialog), nameof(OnClosedDir));
    }

    void OnClosedDir() {
        folderDialog.DisconnectDir(this, nameof(SetDirDialog), nameof(OnClosedDir));
    }

    void SetDirDialog(string dir) {
        SetDir(dir);
        OnClosedDir();
    }

    public void SetDir(string dir) {
        Settings.StorePath = dir;
        storagePathLabel.Text = dir;
        Settings.Save();
    }

    void ModifiedTag() {
        Settings.ClearTags();
        for (int i = 0; i < tagList.Count; i++) {
            var tagControl = tagList[i] as ReleaseTagControl;
            var keys = new string[tagControl.Count];
            for (int j = 0; j < tagControl.Count; j++) {
                keys[j] = (tagControl[j].Data as LineEdit).Text;
            }
            Settings.SetTag(tagControl.TagName.Text, keys);
        }
        Settings.Save();
    }
}

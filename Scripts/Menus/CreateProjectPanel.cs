using Godot;

public class CreateProjectPanel : Control {
    [Export] NodePath nameNode;
    [Export] NodePath locationNode;
    [Export] NodePath locationButtonNode;
    [Export] NodePath createButtonNode;
    [Export] NodePath versionSelectorNode;
    [Export] NodePath folderDialogNode;

    LineEdit name;
    LineEdit location;
    OptionButton versionSelector;
    string[] installIDs;
    FileDialog folderDialog;

    public override void _EnterTree() {
        base._EnterTree();
        name = GetNode<LineEdit>(nameNode);
        location = GetNode<LineEdit>(locationNode);
        GetNode<Button>(createButtonNode).OnPressed(this, nameof(CreateProject));
        versionSelector = GetNode<OptionButton>(versionSelectorNode);
        folderDialog = GetNode<FileDialog>(folderDialogNode);
        GetNode<Button>(locationButtonNode).OnPressed(this, nameof(ChooseLocation));
    }

    public void ShowPanel() {
        int i = 0;
        versionSelector.Selected = 0;
        installIDs = new string[Installs.Count];
        foreach (var install in Installs.GetInstalls()) {
            installIDs[i] = install.ID;
            versionSelector.AddItem(install.FullVersionName);
            if (install.ID == Settings.DefaultInstall) {
                versionSelector.Selected = i;
            }
            i++;
        }
        location.Text = Settings.LastProjectFolder;
        Show();
    }

    public void HidePanel() {
        Hide();
    }

    void CloseLocationPopup() {
        folderDialog.DisconnectDir(this, nameof(SetLocation), nameof(CloseLocationPopup));
    }

    void ChooseLocation() {
        folderDialog.SelectDir(this, nameof(SetLocation), nameof(CloseLocationPopup));
        folderDialog.CurrentDir = location.Text;
    }

    void SetLocation(string dir) {
        location.Text = dir;
        CloseLocationPopup();
    }

    bool ValidName() {
        return !string.IsNullOrWhiteSpace(name.Text);
    }

    void CreateProject() {
        if (!ValidName()) return;
        var dir = System.IO.Path.Combine(location.Text, name.Text);
        System.IO.Directory.CreateDirectory(dir);
        Settings.LastProjectFolder = location.Text;
        Settings.Save();
        var projectPath = System.IO.Path.Combine(dir, $"project.godot");
        var install = Installs.GetInstall(installIDs[versionSelector.Selected]);
        var config = new ConfigFile();
        config.SetValue("application", "config/name", name.Text);
        config.Save(projectPath);
        
        ProjectData.LaunchProject(install, projectPath);

        var project = new Project();
        project.ProjectPath = projectPath;
        project.VersionID = installIDs[versionSelector.Selected];
        Projects.Add(project);

        Hide();
    }
}

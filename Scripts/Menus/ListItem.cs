using Godot;

public class ListItem : PanelContainer {
    [Signal] public delegate void Modified();

    [Export] NodePath buttonDeleteNode;
    [Export] NodePath dataNode;

    public Node Data;

    public override void _EnterTree() {
        base._EnterTree();
        GetNode<Button>(buttonDeleteNode).Connect("pressed", this, nameof(Delete));
        Data = GetNode(dataNode);
    }

    void Delete() {
        QueueFree();
        EmitSignal(nameof(Modified));
    }

    public void ModifiedString(string value) {
        EmitSignal(nameof(Modified));
    }
}

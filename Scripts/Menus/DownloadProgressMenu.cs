using Godot;
using System.IO.Compression;

public class DownloadProgressMenu : Control {
    const string TEMP_PATH = "temp/tmp_godot.zip";

    [Export] NodePath progressBarNode;

    ProgressBar progressBar;
    HTTPRequest downloadRequest;
    int lastDownloaded;
    Install install;

    public override void _EnterTree() {
        base._EnterTree();
        progressBar = GetNode<ProgressBar>(progressBarNode);
        downloadRequest = Network.CreateRequest(this,
            nameof(OnDownloadCompleted));
    }

    public static string FindExecutable(string dir) {
        foreach (var file in System.IO.Directory.GetFiles(dir)) {
            var filepath = System.IO.Path.Combine(dir, file);
            if (Platform.IsExecutable(filepath)) return filepath;
        }
        foreach (var file in System.IO.Directory.GetDirectories(dir)) {
            var filepath = System.IO.Path.Combine(dir, file);
            if (Platform.IsExecutable(filepath)) {
#if GODOT_MACOS || GODOT_OSX
                return System.IO.Path.Combine(filepath, "Contents", "MacOS", "Godot");
#else
                return filepath;
#endif
            }
            if (System.IO.Directory.Exists(filepath)) {
                var exec = FindExecutable(filepath);
                if (!string.IsNullOrWhiteSpace(exec)) return exec;
            }
        }
        return "";
    }

    public void OnDownloadCompleted(int result, int response_code, string[] headers, byte[] body) {
        Visible = false;
        if (result != (int) HTTPRequest.Result.Success) return;
        
        // Move download to its correct directory
        string dir = install.GenerateInstallDir();
        if (!System.IO.Directory.Exists(dir)) {
            System.IO.Directory.CreateDirectory(dir);
        }
        ZipFile.ExtractToDirectory(downloadRequest.DownloadFile, dir);
        System.IO.File.Delete(downloadRequest.DownloadFile);

        install.ExecutablePath = FindExecutable(dir);
        install.InstallPath = dir;
        // Make file executable
#if GODOT_X11
        if (!string.IsNullOrWhiteSpace(install.ExecutablePath)) {
            OS.Execute("chmod", new string[] { "+x", install.ExecutablePath });
        }
#endif

        Installs.AddInstall(install);
    }

    public override void _Process(float delta) {
        base._Process(delta);

        var size = downloadRequest.GetBodySize();
        var downloaded = downloadRequest.GetDownloadedBytes();
        if (lastDownloaded < downloaded) {
            progressBar.Value = Mathf.RoundToInt(downloaded * 100f / size);
            lastDownloaded = downloaded;
        }
    }

    public void StartDownload(Install install) {
        this.install = install;
        Visible = true;
        downloadRequest.DownloadFile = System.IO.Path.Combine(Settings.StorePath, TEMP_PATH);
        var dir = System.IO.Path.GetDirectoryName(downloadRequest.DownloadFile);
        if (!System.IO.Directory.Exists(dir)) {
            System.IO.Directory.CreateDirectory(dir);
        }
        GD.Print($"Downloading {install.URL} to {downloadRequest.DownloadFile}");
        downloadRequest.Request(VersionsFetcher.URL(install.URL));
        lastDownloaded = 0;
        progressBar.Value = 0;
    }

    public void Cancel() {
        Visible = false;
        downloadRequest.CancelRequest();
        if (System.IO.File.Exists(downloadRequest.DownloadFile)) {
            System.IO.File.Delete(downloadRequest.DownloadFile);
        }
    }
}

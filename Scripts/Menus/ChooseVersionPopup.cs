using Godot;

public class ChooseVersionPopup : Control {
    [Export] NodePath loadingMessagePath;
    [Export] NodePath rootNode;
    [Export] NodePath downloadProgressMenuNode;
    [Export] PackedScene versionListItem;

    VersionsFetcher fetcher;
    Install install;
    Control loadingMessage;
    Node root;
    DownloadProgressMenu downloadProgressMenu;

    public override void _Ready() {
        base._Ready();
        root = GetNode(rootNode);
        downloadProgressMenu = GetNode<DownloadProgressMenu>(downloadProgressMenuNode);
        fetcher = new VersionsFetcher();
        AddChild(fetcher);
        loadingMessage = GetNode<Control>(loadingMessagePath);
        loadingMessage.Visible = true;
        
        fetcher.OnFetchedVersion += GotVersionReleases;
        fetcher.OnFetchedReleases += GotVersionsCallback;
    }

    void GotVersionReleases(VersionRemote version) {
        var versionList = versionListItem.Instance<VersionList>();
        root.AddChild(versionList);
        versionList.SetVersion(version);
        root.MoveChild(loadingMessage, root.GetChildCount() - 1);
    }

    void GotVersionsCallback() {
        loadingMessage.Visible = false;
    }

    public void Reset() {
        for (int i = 0; i < root.GetChildCount(); i++) {
            root.GetChild(i).QueueFree();
        }
    }

    string GetFileName(string url) {
        var parts = url.Split("/");
        for (int i = parts.Length - 1; i >= 0; i--) {
            var part = parts[i];
            if (!string.IsNullOrWhiteSpace(part)) return part;
        }
        return parts[parts.Length - 1];
    }

    void RefreshVersionsList() {
        loadingMessage.Visible = true;
        fetcher.StartFetching();
    }

    public void DownloadRelease(Install install) {
        downloadProgressMenu.StartDownload(install);
    }

    public void Back() {
        Visible = false;
    }
}

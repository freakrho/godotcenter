using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

public static class Installs {
    class VersionComparer : IComparer<string> {
        public int Compare(string x, string y) {
            return -x.CompareTo(y);
        }
    }

    const string INSTALLS_PATH = "user://installs.json";

    static JArray data;

    public static event System.Action OnUpdate;

    public static bool Dirty {
        get {
            foreach (Install install in data) {
                if (install.Dirty) return true;
            }
            return false;
        }
    }
    public static int Count => data.Count;

    public static void Load() {
        data = DataManager.Load<JArray>(INSTALLS_PATH);
        Sort();
        
        if (Dirty) {
            Save();
        }
    }

    static void LoadInstallData() {
        for (int i = data.Count - 1; i >= 0; i--) {
            var installData = data[i];
            if (!(installData is JObject)) {
                data.RemoveAt(i);
                continue;
            }
            data[i] = new Install((installData as JObject));
        }
    }

    public static void Save() {
        DataManager.Save(INSTALLS_PATH, data);
    }
    
    static void Sort() {
        data = new JArray(data.OrderBy(
            (install) => { return (string) install[Install.VERSION]; },
            new VersionComparer()
        ));
        LoadInstallData();
    }

    public static IEnumerable<Install> GetInstalls() {
        foreach (Install install in data) yield return install;
    }

    public static Install Get(int i) {
        return data[i] as Install;
    }

    public static void AddInstall(Install install) {
        data.Add(install);
        Sort();
        Installs.Save();
        OnUpdate?.Invoke();
    }

    public static void DeleteInstall(Install install) {
        if (System.IO.Directory.Exists(install.InstallPath)) {
            System.IO.Directory.Delete(install.InstallPath, true);
        }
        data.Remove(install);
        OnUpdate?.Invoke();
        Installs.Save();
    }

    public static Install GetInstall(string guid) {
        foreach (Install install in data) {
            if (install.ID == guid) return install;
        }
        return null;
    }
}

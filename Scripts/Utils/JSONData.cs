using Newtonsoft.Json.Linq;

public class JSONData : JObject {
    public bool Dirty;
    
    public JSONData() : base() { }
    
    public JSONData(JObject obj) : base(obj) { }

    public void SetString(string key, string value) {
        this[key] = value;
        Dirty = true;
    }
}

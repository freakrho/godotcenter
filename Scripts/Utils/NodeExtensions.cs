using Godot;

public static class NodeExtensions {
    public static SignalAwaiter YieldFrame(this Godot.Object obj) {
        return obj.ToSignal(Engine.GetMainLoop(), "idle_frame");
    }

    public static void QueueFreeChildren(this Node node) {
        for (int i = 0; i < node.GetChildCount(); i++) {
            node.GetChild(i).QueueFree();
        }
    }

    public static void SelectFile(
        this FileDialog dialog, Node node,
        string fileSelectedMethod, string popupHideMethod
    ) {
        dialog.Connect("file_selected", node, fileSelectedMethod);
        dialog.Connect("popup_hide", node, popupHideMethod);
        dialog.PopupCentered();
    }

    public static void DisconnectFile(
        this FileDialog dialog, Node node,
        string fileSelectedMethod, string popupHideMethod
    ) {
        dialog.Disconnect("file_selected", node, fileSelectedMethod);
        dialog.Disconnect("popup_hide", node, popupHideMethod);
    }

    public static void SelectDir(
        this FileDialog dialog, Node node,
        string dirSelectedMethod, string popupHideMethod
    ) {
        dialog.Connect("dir_selected", node, dirSelectedMethod);
        dialog.Connect("popup_hide", node, popupHideMethod);
        dialog.PopupCentered();
    }

    public static void DisconnectDir(
        this FileDialog dialog, Node node,
        string dirSelectedMethod, string popupHideMethod
    ) {
        dialog.Disconnect("dir_selected", node, dirSelectedMethod);
        dialog.Disconnect("popup_hide", node, popupHideMethod);
    }

    public static void OnPressed(this Button button, Node node, string method) {
        button.Connect("pressed", node, method);
    }
}

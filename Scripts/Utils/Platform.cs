public static class Platform {
    public static bool ThisPlatformOrNone(string url) {
        url = url.ToLower();
        var engine = Install.GetEngineVersion(url);
#if GODOT_WINDOWS
        return
            engine == EngineVersion.Windows32
            ||
            engine == EngineVersion.Windows64
#elif GODOT_LINUXBSD || GODOT_X11
        return
            engine == EngineVersion.Linux32
            ||
            engine == EngineVersion.Linux64
            ||
            engine == EngineVersion.Linux64Headless
#elif GODOT_SERVER
        return engine == EngineVersion.LinuxServer
#elif GODOT_MACOS || GODOT_OSX
        return engine == EngineVersion.OSX
#elif GODOT_ANDROID
        return engine == EngineVersion.Android
#else
        return true
#endif
        || engine == EngineVersion.None;
    }

    public static bool ThisPlatform(string url) {
        url = url.ToLower();
        var engine = Install.GetEngineVersion(url);
#if GODOT_WINDOWS
        return
            engine == EngineVersion.Windows32
            ||
            engine == EngineVersion.Windows64;
#elif GODOT_LINUXBSD || GODOT_X11
        return
            engine == EngineVersion.Linux32
            ||
            engine == EngineVersion.Linux64
            ||
            engine == EngineVersion.Linux64Headless;
#elif GODOT_SERVER
        return engine == EngineVersion.LinuxServer;
#elif GODOT_MACOS || GODOT_OSX
        return engine == EngineVersion.OSX;
#elif GODOT_ANDROID
        return engine == EngineVersion.Android;
#else
        return true;
#endif
    }

    public static bool IsExecutable(string file) {
#if GODOT_WINDOWS
        return file.ToLower().EndsWith(".exe");
#elif GODOT_LINUXBSD || GODOT_X11
        return file.EndsWith(".64") || file.EndsWith(".32");
#elif GODOT_MACOS || GODOT_OSX
        return file.ToLower().EndsWith(".app");
#elif GODOT_ANDROID
        return file.ToLower().EndsWith(".apk");
#else
        return false;
#endif
    }
}

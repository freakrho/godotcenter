using System;
using Newtonsoft.Json.Linq;

public class Install : JSONData {
    public const string VERSION = "version";
    const string TYPE = "type";
    const string EXEC_PATH = "exec_path";
    const string INSTALL_PATH = "install_path";
    const string MONO = "mono";
    const string URL_KEY = "url";
    const string TAGS = "tags";
    const string ID_KEY = "id";

    public string ID {
        get => (string) this[ID_KEY];
        set { SetString(ID_KEY, value); }
    }
    public string Version {
        get => (string) this[VERSION];
        set { SetString(VERSION, value); }
    }
    public string ReleaseType {
        get => (string) this[TYPE];
        set { SetString(TYPE, value); }
    }
    public string InstallPath {
        get => (string) this[INSTALL_PATH];
        set { SetString(INSTALL_PATH, value); }
    }
    public string ExecutablePath {
        get => (string) this[EXEC_PATH];
        set { SetString(EXEC_PATH, value); }
    }
    public bool Mono {
        get {
            if (!ContainsKey(MONO)) return false;
            return (bool) this[MONO];
        }
        set {
            this[MONO] = value;
            Dirty = true;
        }
    }

    public string URL {
        get => (string) this[URL_KEY];
        set { SetString(URL_KEY, value); }
    }

    public int TagsLength => (this[TAGS] as JArray).Count;
    public string GetTag(int index) {
        return (string) this[TAGS][index];
    }
    public void AddTag(string tag) {
        if (!ContainsKey(TAGS)) {
            this[TAGS] = new JArray();
        }
        (this[TAGS] as JArray).Add(tag);
    }

    public string FullVersionName {
        get {
            var name = $"v{Version} {ReleaseType}";
            if (Mono) {
                name += " Mono";
            }
            for (int i = 0; i < TagsLength; i++) {
                name += $" {GetTag(i)}";
            }
            return name;
        }
    }

    public string GenerateInstallDir() {
        var dir = Version;
        if (!string.IsNullOrWhiteSpace(ReleaseType)) {
            dir += $"_{ReleaseType}";
        }
        if (Mono) {
            dir += "_mono";
        }
        return System.IO.Path.Combine(Settings.StorePath, dir);
    }

    public static EngineVersion GetEngineVersion(string filename) {
        filename = filename.ToLower();

        if (filename.Contains("linux_server")) return EngineVersion.LinuxServer;
        if (filename.Contains("x11") || filename.Contains("linux")) {
            if (filename.Contains("32")) return EngineVersion.Linux32;
            if (filename.Contains("headless")) return EngineVersion.Linux64Headless;
            return EngineVersion.Linux64;
        }
        if (filename.Contains("win")) {
            if (filename.Contains("32")) return EngineVersion.Windows32;
            return EngineVersion.Windows64;
        }
        if (filename.Contains("osx")) return EngineVersion.OSX;
        if (filename.EndsWith(".apk")) return EngineVersion.Android;

        return EngineVersion.None;
    }

    public void GenerateID() {
        ID = Guid.NewGuid().ToString();
    }

    public Install() {
        if (!ContainsKey(TAGS)) {
            this[TAGS] = new JArray();
        }
        if (!ContainsKey(ID_KEY)) {
            GenerateID();
        }
    }
    public Install(JObject obj) : base(obj) {
        if (!ContainsKey(TAGS)) {
            this[TAGS] = new JArray();
        }
        if (!ContainsKey(ID_KEY)) {
            GenerateID();
        }
    }
}

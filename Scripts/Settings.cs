using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Newtonsoft.Json.Linq;

public class ReleaseTag {
    public string Name;
    public string[] SearchKeys;
}

public static class Settings {
    public const string SETTINGS_PATH = "user://settings.json";
    const string STORE_PATH = "StorePath";
    const string TAGS = "Tags";
    const string DEFAULT_INSTALL = "DefaultInstall";
    const string LAST_PROJECT_FOLDER = "LastProjectFolder";

    static JObject data;

    static Settings() {
        Load();
    }

    public static void Load() {
        var file = new File();
        if (file.FileExists(SETTINGS_PATH)) {
            data = DataManager.Load<JObject>(SETTINGS_PATH);
        } else {
            data = new JObject();
        }
        ValidateValues();
    }

    static void ValidateValues() {
        if (!data.ContainsKey(STORE_PATH)) {
            StorePath = "~/.godot-hub/";
        }
        if (!data.ContainsKey(TAGS)) {
            data[TAGS] = new JObject();
            SetTag("64-bit", new string[]{ "64" });
            SetTag("32-bit", new string[]{ "32" });
            SetTag("Headless", new string[]{ "headless" });
        }
    }

    public static void Save() {
        Main.Inst.SaveSettings();
    }

    public static void DoSave() {
        DataManager.Save(SETTINGS_PATH, data, false);
    }

    #region Settings
    public static string StorePath {
        get { return (string) data[STORE_PATH]; }
        set { data[STORE_PATH] = value; }
    }
    public static IEnumerable<ReleaseTag> Tags() {
        JObject tags = (JObject) data[TAGS];
        foreach (var pair in tags) {
            yield return new ReleaseTag {
                Name = pair.Key,
                SearchKeys = pair.Value.Values<string>().ToArray(),
            };
        }
    }
    public static void SetTag(string name, string[] keys) {
        var tag = new JArray(keys);
        data[TAGS][name] = tag;
        GD.Print($"Tag {name}: {tag}");
    }
    public static void ClearTags() {
        data[TAGS] = new JObject();
    }
    public static string DefaultInstall {
        get => (string) data[DEFAULT_INSTALL];
        set { data[DEFAULT_INSTALL] = value; }
    }
    public static string LastProjectFolder {
        get => (string) data[LAST_PROJECT_FOLDER];
        set { data[LAST_PROJECT_FOLDER] = value; }
    }
    #endregion
}

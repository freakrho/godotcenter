using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Godot;
using System.Net;
using System.Net.Security;

public class Release : IComparable<Release> {
    public string Type;
    public string Name;
    public string URL;
    public bool Mono;
    public bool Done;
    public string[] Tags;

    int Priority {
        get {
            if (Type == "Alpha") return 1;
            if (Type == "Beta") return 2;
            if (GetRC() > 0) return 3;
            if (Type == "Release") return 4;
            if (Type == "Stable") return 5;
            return 0;
        }
    }
    int GetRC() {
        foreach (var tag in Tags) {
            if (tag.BeginsWith("RC")) {
                if (int.TryParse(tag.Substring(2), out var rc)) return rc;
            }
        }
        return 0;
    }

    public int CompareTo(Release other) {
        var priority = Priority;
        var otherPriority = other.Priority;
        if (priority < 3 && otherPriority < 3) return priority.CompareTo(otherPriority);

        // Release candidates
        var rc = GetRC();
        var otherRC = other.GetRC();
        if (rc > 0 && otherRC > 0) return rc.CompareTo(otherRC);

        return priority.CompareTo(otherPriority);
    }
}

public class VersionRemote {
    public string Version;
    public List<Release> Releases = new List<Release>();
}

public class VersionsFetcher : Node {
    class Request {
        public Uri URI;
        public Action<string> Callback;

        public override string ToString() {
            return URI.AbsoluteUri;
        }
    }

    const int MAX_CONNECTIONS = 10;
    const string HOST = "https://downloads.tuxfamily.org";
    const string ROOT = "godotengine";

    bool mono;
    RegEx versionRegex = new RegEx();
    RegEx linkRegex = new RegEx();
    Action<string>[] requestCallbacks;
    WebClient[] clients;
    WebClient standaloneClient;
    Queue<Request> requests;

    public List<VersionRemote> Versions = new List<VersionRemote>();
    public event Action OnFetchedReleases;
    public event Action<VersionRemote> OnFetchedVersion;
    public bool Busy { get; set; }

    public override void _EnterTree() {
        base._EnterTree();
        ServicePointManager.ServerCertificateValidationCallback = new
            RemoteCertificateValidationCallback(delegate { return true; });
        versionRegex.Compile("(?<=>)(?:[0-9]\\.){1,2}[0-9](?=<)");
        linkRegex.Compile("<a\\s[^>]*\\bhref=\"([^#\"][^\"]*)\"");
        clients = new WebClient[MAX_CONNECTIONS];
        requestCallbacks = new Action<string>[MAX_CONNECTIONS];
        for (int i = 0; i < MAX_CONNECTIONS; i++) {
            var client = new WebClient();
            clients[i] = client;
            var index = i;
            client.DownloadStringCompleted += (obj, e) => {
                if (!e.Cancelled && e.Error == null) {
                    // GD.Print($"Got request {index} {e.UserState}");
                    requestCallbacks[index](e.Result);
                } else {
                    GD.Print($"Error downloading {e.UserState} {e.Error}");
                }
                CheckClients();
            };
        }
        standaloneClient = new WebClient();
        requests = new Queue<Request>();

        FetchVersions();
    }

    public static string URL(string url) {
        return HOST + "/" + ROOT + "/" + url;
    }

    int GetClient() {
        for (int i = 0; i < clients.Length; i++) {
            if (!clients[i].IsBusy) return i;
        }
        return -1;
    }

    bool NoClientsBusy() {
        foreach (var client in clients) {
            if (client.IsBusy) return false;
        }
        return true;
    }

    void CheckClients() {
        while (requests.Count > 0) {
            var clientIndex = GetClient();
            if (clientIndex < 0) return;
            var request = requests.Dequeue();
            var client = clients[clientIndex];
            requestCallbacks[clientIndex] = request.Callback;
            // GD.Print($"Requesting {clientIndex} {request.URI.AbsoluteUri}");
            client.DownloadStringAsync(request.URI, request);
        }
    }

    void FetchRequest(string url, Action<string> callback) {
        var request = new Request {
            URI = new Uri(URL(url)),
            Callback = callback,
        };
        // GD.Print($"Request {url}");
        requests.Enqueue(request);
        CheckClients();
    }

    async Task<string> Fetch(string url) {
        return await standaloneClient.DownloadStringTaskAsync(URL(url));
    }

    public void StartFetching() {
        if (Busy) return;
        FetchVersions();
    }

    async void FetchVersions() {
        Busy = true;
        var body = await Fetch("");

        foreach (RegExMatch match in versionRegex.SearchAll(body)) {
            var versionText = match.GetString();
            var version = new VersionRemote {
                Version = versionText,
            };
            version.Releases.Add(new Release {
                URL = $"{versionText}/",
            });
            Versions.Add(version);
        }

        Versions.Reverse();
        await FetchAllReleases();
        // foreach (var version in Versions) {
        //     GD.Print(version.Version + ":");
        //     foreach (var release in version.Releases) {
        //         GD.Print($"\t{release.URL}");
        //     }
        //     await this.YieldFrame();
        // }
        for (int i = Versions.Count - 1; i >= 0; i--) {
            var version = Versions[i];

            if (version.Releases.Count == 0) {
                Versions.RemoveAt(i);
            }
        }
        Busy = false;
        OnFetchedReleases?.Invoke();
    }

    string FindReleaseType(string url) {
        var lower = url.ToLower();
        if (lower.Contains("stable")) return "Stable";
        if (lower.Contains("release")) return "Release";
        if (lower.Contains("beta")) return "Beta";
        if (lower.Contains("alpha")) return "Alpha";
        return "";
    }

    bool IsMono(string url) {
        return url.ToLower().Contains("mono/");
    }

    async Task FetchAllReleases() {
        foreach (var version in Versions) {
            await FetchVersionReleases(version);
            for (int j = version.Releases.Count - 1; j >= 0; j--) {
                if (!version.Releases[j].Done) {
                    version.Releases.RemoveAt(j);
                }
            }
            if (version.Releases.Count > 0) {
                version.Releases.Sort();
                version.Releases.Reverse();
                OnFetchedVersion?.Invoke(version);
            }
        }
    }

    static string[] GetTags(string url) {
        var tags = new List<string>();
        // First check if it's a release candidate
        var parts = url.ToLower().Split('/');
        foreach (var part in parts) {
            if (part.StartsWith("rc")) {
                tags.Add(part.ToUpper());
            }
        }

        foreach (var tag in Settings.Tags()) {
            foreach (var key in tag.SearchKeys) {
                if (url.Contains(key)) {
                    tags.Add(tag.Name);
                    break;
                }
            }
        }
        return tags.ToArray();
    }

    async Task FetchVersionReleases(VersionRemote version) {
        bool pending = false;

        var length = version.Releases.Count;
        for (int i = 0; i < length; i++) {
            var release = version.Releases[i];
            if (release.Done) continue;

            var url = release.URL;
            // GD.Print($"\tFor {url}");
            bool first = false;

            FetchRequest(url, (body) => {
                foreach (RegExMatch match in linkRegex.SearchAll(body)) {
                    var itemUrl = (string) match.Strings[match.Strings.Count - 1];
                    if (itemUrl == "../") continue;
                    bool isDir = itemUrl.EndsWith("/");
                    if (
                        !Platform.ThisPlatformOrNone(itemUrl)
                        ||
                        (!isDir && !Platform.ThisPlatform(itemUrl))
                    ) continue;
                    // GD.Print($"\t\tGot: {itemUrl}");
                    Release thisRelease;
                    if (!first) {
                        first = true;
                        thisRelease = release;
                    } else {
                        thisRelease = new Release {
                            URL = url,
                        };
                        version.Releases.Add(thisRelease);
                    }

                    thisRelease.URL += itemUrl;
                    if (isDir) {
                        pending = true;
                    } else { // File
                        thisRelease.Done = true;
                        thisRelease.Type = FindReleaseType(thisRelease.URL);
                        thisRelease.Mono = IsMono(thisRelease.URL);
                        thisRelease.Name = itemUrl;
                        thisRelease.Tags = GetTags(thisRelease.URL);
                    }
                }
            });
        }
        while (!NoClientsBusy()) await this.YieldFrame();
        if (pending) {
            await FetchVersionReleases(version);
        }
    }
}

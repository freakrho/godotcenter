using Godot;

public static class Network {
    public static HTTPRequest CreateRequest(Node target, string callback) {
        var request = new HTTPRequest();
        target.AddChild(request);
        request.Connect("request_completed", target, callback);
        return request;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using Godot;
using Newtonsoft.Json.Linq;

public class Project : JSONData {
    const string PATH_KEY = "Path";
    const string VERSION_KEY = "VersionID";
    const string NAME_KEY = "Name";
    const string FAVORITE = "Favorite";
    const string MODIFIED = "Modified";

    public string ProjectPath {
        get => (string) this[PATH_KEY];
        set { SetString(PATH_KEY, value); }
    }
    public string VersionID {
        get => (string) this[VERSION_KEY];
        set { SetString(VERSION_KEY, value); }
    }
    public string ProjectName {
        get {
            if (!ContainsKey(NAME_KEY)) {
                var config = new ConfigFile();
                config.Load(ProjectPath);
                SetString(NAME_KEY, (string) config.GetValue("application", "config/name"));
            }
            return (string) this[NAME_KEY];
        }
    }
    public bool Favorite {
        get {
            if (!ContainsKey(FAVORITE)) {
                Favorite = false;
            }
            
            return (bool) this[FAVORITE];
        }
        set {
            this[FAVORITE] = value;
            Dirty = true;
        }
    }
    public DateTime Modified {
        get {
            if (!ContainsKey(MODIFIED)) {
                this[MODIFIED] = DateTime.UtcNow;
                Dirty  = true;
            }
            return (DateTime) this[MODIFIED];
        }
        set {
            this[MODIFIED] = value;
            Dirty = true;
        }
    }

    public static bool GetTimeSince(DateTime objDateTime, out string text) {
        TimeSpan ts = DateTime.UtcNow.Subtract(objDateTime);
        int days = ts.Days;
        int weeks = days / 7;
        int months = days / 30;

        if (months > 0) {
            text = $"{months} month{(months > 1 ? "s" : "")}";
            return true;
        }
        if (weeks > 0) {
            text = $"{weeks} week{(weeks > 1 ? "s" : "")}";
            return true;
        }
        if (days > 0) {
            text = $"{days} day{(days > 1 ? "s" : "")}";
            return true;
        }

        text = "today";
        return false;
    }

    public string TimeAgo() {
        if (GetTimeSince(Modified, out var text)) {
            return $"{text} ago";
        }
        return text;
    }

    public void ModifiedNow() {
        Modified = DateTime.UtcNow;
    }

    public Project(JObject obj) : base(obj) { }
    public Project() : base() { }
}

public class ProjectEnum : IEnumerator<Project> {
    int position = -1;

    public Project Current => List[position] as Project;
    object IEnumerator.Current => Current;
    public JArray List;

    public Project this[int i] => List[i] as Project;
    public int Count => List.Count;

    public ProjectEnum(JArray list) {
        this.List = list;
    }

    public void Dispose() {
    }

    public bool MoveNext() {
        position++;
        return position < List.Count;
    }

    public void Reset() {
        position = -1;
    }
}

public class ProjectList : IEnumerable<Project> {
    JArray list;

    public int Count => list.Count;
    public Project this[int i] => list[i] as Project;
    bool dirty;
    public bool Dirty {
        get {
            if (dirty) return true;
            foreach (Project project in this) {
                if (project.Dirty) return true;
            }
            return false;
        }
    }

    public void Add(Project project) {
        list.Add(project);
        dirty = true;
    }

    public void Remove(Project project) {
        list.Remove(project);
        dirty = true;
    }

    public void Load(string path) {
        list = DataManager.Load<JArray>(path);
        for (int i = list.Count - 1; i >= 0; i--) {
            var projectData = list[i];
            if (!(projectData is JObject)) {
                list.RemoveAt(i);
                continue;
            }
            list[i] = new Project((projectData as JObject));
        }
    }

    public void Save(string path) {
        DataManager.Save(path, list);
        dirty = false;
    }

    public IEnumerator<Project> GetEnumerator() {
        return new ProjectEnum(list);
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return (IEnumerator) GetEnumerator();
    }

    public void MakeFirst(Project project) {
        list.Remove(project);
        list.AddFirst(project);
        dirty = true;
    }
}

public static class Projects {
    const string PATH = "user://projects.json";

    static ProjectList list;
    
    public static event System.Action OnUpdate;
    public static IEnumerable<Project> Enumerable => list;
    public static bool Dirty => list.Dirty;

    static Projects() {
        list = new ProjectList();
        Load();
    }

    public static void Add(Project project) {
        list.Add(project);
        OnUpdate?.Invoke();
    }

    public static void Remove(Project project) {
        list.Remove(project);
        OnUpdate?.Invoke();
    }

    public static void MakeFirst(Project project) {
        list.MakeFirst(project);
        OnUpdate?.Invoke();
    }

    public static void Load() {
        list.Load(PATH);
        if (list.Dirty) {
            Save();
        }
    }

    public static void Save() {
        list.Save(PATH);
    }
}

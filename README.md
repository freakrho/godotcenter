<div align="center">
<img src="logo.svg" alt="Logo" width="256px" height="256px" />

# GodotCenter

**A place to keep all your [Godot](https://godotengine.org) projects and engine versions organized.**

[![matrix](https://img.shields.io/badge/chat-matrix-0dbd8b)](https://matrix.to/#/#godot-center:matrix.org)

<img src="Media/Screenshot01.png" alt="Logo" width="300px" />
<img src="Media/Screenshot02.png" alt="Logo" width="300px" />
<img src="Media/Screenshot03.png" alt="Logo" width="300px" />

</div>

**GodotCenter** is meant as a familiar app for people coming from Unity using UnityHub, it is designed to mimic all of its functionality.

## Key Features

- Download different versions of godot directly from the app
- Keep all your projects in one place and open them with the godot version you prefer

## How to get it

There is a linux executable in [itch.io](https://freakrho.itch.io/godotcenter) (I'm still figuring out how to upload releases to gitlab, if you want to help with this, hit me up).

Otherwise the source is offered as a Godot (mono) project so you can build it yourself.
